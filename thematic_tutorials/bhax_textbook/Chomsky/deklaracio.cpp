#include <stdio.h>

int *g() //egészre mutató mutatót visszaadó függvény
{
	return 0;
}

int func(int a, int b)
{
	return 0;
}

// egészet visszaadó és két egészet kapó függvényre mutatót visszaadó, egészet kapó függvény

int (* func2(int c))(int a , int b)
{
	int (*i)(int, int) = func;
	return i;
}

int main()
{
	int a = 0; //egész

	int *b;
	b = &a; //egészre mutató mutató

	int &c = a; //egész referenciája

	int d[10]; //egészek tömbje

	int (&e)[10] = d; //tömb referenciája

	int *f[3]; //egészekre mutató mutatók tömbje

	int * (*h)(); // egészre mutató mutatót visszaadó függvényre mutató mutató

	//függvénymutató egy egészet visszaadó és két egészet kapó függvényre mutató mutatót
	//visszaadó, egészet kapó függvény
	int (*(*j)(int z))(int x, int y) = &func2;

	//---------------------------------------------------


}