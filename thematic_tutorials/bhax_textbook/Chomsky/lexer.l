%{
	#include <string.h>
	int count = 0;
%}

%%
-{0,1}[0-9]*,{0,1}[0-9]*		++count;
%%

int main()
{
	yylex();
	printf("A valós számok száma: %d\n", count);
}