%{
	#include <string.h>
%}

%%
a|A		printf("4");
b|B		printf("|3");
c|C		printf("<");
d|D		printf("|)");
e|E		printf("3");
f|F		printf("|=");
g|G		printf("6");
h|H		printf("|-|");
i|I    		printf("1");
j|J		printf("_|");
k|K		printf("|<");
l|L		printf("|_");
m|M		printf("|\\/|");
n|N		printf("|\\|");
o|O		printf("o");
p|P		printf("|o");
q|Q		printf("O_");
r|R		printf("|2");
s|S		printf("5");
t|T		printf("7");
u|U		printf("|_|");
v|V		printf("\\/");
w|W		printf("VV");
x|X		printf("%%");
y|Y		printf("'/");
z|Z		printf("2");
%%

int main()
{
	yylex();
}
