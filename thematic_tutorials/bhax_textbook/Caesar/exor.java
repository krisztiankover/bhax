import java.io.*;

public class exor
{
	public static void main(String args[]) throws Exception
	{
		BufferedReader input = new BufferedReader(new FileReader(args[0]));
		FileWriter output = new FileWriter(args[1]);
		int key = Integer.parseInt(args[2]);

		int ch = 0;
		int a = 0;
		String text = "";

		while ((ch = input.read()) != -1)
		{
			a = (char)( ch ^ key);
			output.write(a);
			//text += (char)a;
		}

		input.close();
		output.flush();
		output.close();

		//System.out.println(text);
	}
}