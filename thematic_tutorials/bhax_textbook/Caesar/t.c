#include <stdio.h>
#include <string.h>
#include <stdbool.h>

bool word_length(char text[])
{
	int ws_counter = 0;

	for (int i = 0; i < strlen(text); i++)
	{
		if (text[i] == ' ')
			ws_counter++;
	}

	double avg_length = (double) strlen(text) / ws_counter;

	if (avg_length < 9 && avg_length > 6)
		return true;
	return false;
}

bool word_check(char text[])
{
	if (strstr(text, "hogy") != NULL && strstr(text, "nem") != NULL 
		&& strstr(text, "az") != NULL && strstr(text, "ha") != NULL)
		return true;
	return false;
}

int main(int argc, char *argv[])
{
	FILE *input;
	input = fopen(argv[1], "r");

	char text[4000] = "";
	char decr[4000] = "";
	char ch[2] = "";
	ch[0] = fgetc(input);

	while (ch[0] != EOF)
	{
		strcat(text, ch);
		ch[0] = fgetc(input);
	}

	int key = 0;

	for (key = 0; key < 10000; key++)
	{
		for (int i = 0; i < strlen(text); i++)
		{
			decr[i]  = text[i] ^ key;
		}

		if (word_length(decr) && word_check(decr))
		{
			printf("%s\nKulcs: %d\n\n", decr, key);
		}

		strcpy(decr, "");
	}
}