#include <stdio.h>

int main(int argc, char *argv[])
{
	char c;
	char enc_text[1000];

	FILE *inputfile;
	inputfile = fopen(argv[1], "r");

	FILE *outputfile;
	outputfile = fopen(argv[2], "w+");

	int key = argv[3];

	c = fgetc(inputfile);
	while(c != EOF)
	{
		c = c ^ key;

		fputc(c, outputfile);
		c = fgetc(inputfile);
	}

	fclose(inputfile);
	fclose(outputfile);
}