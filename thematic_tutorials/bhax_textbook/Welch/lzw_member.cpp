#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <unistd.h>

using namespace std;

class Tree
{
public:

	class Node
	{
	public:
		char value;
		Node *leftNull;
		Node *rightOne;

		Node(char const z = '/');
	};

	/*Node* newNode(char l)
	{
		Node x;
		Node *p;
		p = &x;
		return p;
		printf("%c node létrehozva\n", l);
	}*/

};

Tree::Node::Node(char z)
{
	value = z;
	leftNull = NULL;
	rightOne = NULL;
}

extern void printOut(Tree::Node *root);

int main(int argc, char const *argv[])
{
	char letter;

	Tree bTree;
	Tree::Node root;
	root = Tree::Node('/');
	Tree::Node *tree;
	tree = &root;

	while(read (0, (void *) &letter, 1))
	{
		write(1, &letter, 1);

		if(letter == '0')
		{
			if(tree -> leftNull == NULL)
			{
				Tree::Node tmp = Tree::Node('0');
				tree -> leftNull = &tmp;
				tree = &root;
			}

			else
			{
				tree = tree -> leftNull;
			}
		}

		else
		{
			if(tree -> rightOne == NULL)
			{
				Tree::Node tmp = Tree::Node('1');
				tree -> rightOne = &tmp;
				tree = &root;
			}

			else
			{
				tree = tree -> rightOne;
			}
		}
	}

	printf("\n");
	cout << root.leftNull -> rightOne -> value;
	//printOut(&root);

	return 0;
}

static int depth = 0;
int max_depth = 0;

void printOut(Tree::Node *e)
{
	if(e != NULL)
	{
		//cout << e -> value;
		depth++;
		if(depth > max_depth)
			max_depth = depth;
		printOut(e -> leftNull);

		for(int i = 0; i < depth; i++)
			printf("---");
		printf("%c(%d)\n", e -> value < 2 ? '0' + e -> value : e -> value, depth);
		printOut(e -> rightOne);
		depth--;
	}
}