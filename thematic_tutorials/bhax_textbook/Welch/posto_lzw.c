#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

typedef struct binaryTree
{
	int value;
	struct binaryTree *leftNull;
	struct binaryTree *rightOne;

} BT, *BT_PTR;

BT_PTR new_e()
{
	BT_PTR p;

	if ((p = (BT_PTR) malloc(sizeof(BT))) == NULL)
	{
		perror("memory");
		exit(EXIT_FAILURE);
	}

	return p;
}

extern void printOut(BT_PTR e);
extern void freeUp(BT_PTR e);

int main(int argc, char **argv)
{
	char letter;

	BT_PTR root = new_e();
	root -> value = '/';
	BT_PTR tree = root;

	while(read (0, (void *) &letter, 1))
	{
		write(1, &letter, 1);
		
		if(letter == '0')
		{
			if(tree -> leftNull == NULL)
			{
				tree -> leftNull = new_e();
				tree -> leftNull -> value = 0;
				tree -> leftNull -> leftNull = tree -> leftNull -> rightOne = NULL;
				tree = root;
			}

			else
			{
				tree = tree -> leftNull;
			}
		}

		else
		{
			if(tree -> rightOne == NULL)
			{
				tree -> rightOne = new_e();
				tree -> rightOne -> value = 1;
				tree -> rightOne -> leftNull = tree -> rightOne -> rightOne = NULL;
				tree = root;
			}

			else
			{
				tree = tree -> rightOne;
			}
		}
	}

	printf("\n");
	printOut(root);
	freeUp(root);
}

static int depth = 0;
int max_depth = 0;

void printOut(BT_PTR e)
{
	if(e != NULL)
	{
		depth++;
		if(depth > max_depth)
			max_depth = depth;
		printOut(e -> leftNull);

		printOut(e -> rightOne);
		
		for(int i = 0; i < depth; i++)
			printf("---");
		printf("%c(%d)\n", e -> value < 2 ? '0' + e -> value : e -> value, depth);
		
		depth--;
	}
}

void freeUp(BT_PTR e)
{
	if(e != NULL)
	{
		freeUp(e -> rightOne);
		freeUp(e -> leftNull);
		free(e);
	}
}